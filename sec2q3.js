const request = require('request');
const j = request.jar();
const cheerio = require('cheerio');

//declare cookie authentication and target url
const cookie = request.cookie('hasCookie=true');
const url = 'https://codequiz.azurewebsites.net/';

main ();

function main (){
    //put cookie in a jar and send it along with the url request
    j.setCookie(cookie, url);
    request({url: url, jar: j}, async function (error, response, body) {

        //use cheerio to parse html string into object and extract table row tag
        const $ = cheerio.load(body);
        let tableRows = $.html('tr');
        
        //loop through table rows and an array of fund names and their NAV
        let output = $(tableRows).slice(1).map(function(i,el) {
            let tds = $(el).find("td");
            return { "Fund name" : tds.eq(0).text(), "NAV" : tds.eq(1).text() };
        }).get();

        //loop through the fund name array to compare with the argument
        for (let i = 0; i < output.length; i++){
            if (output[i]['Fund name'].trim() == process.argv[2].trim()){
                console.log(output[i]['NAV']);
            }
        }
    
    })
    
}
